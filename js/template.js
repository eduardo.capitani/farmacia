function limpar(){
    document.querySelector('main').innerHTML = '';
}
document.getElementById('cadastro').addEventListener('click', function(event){
    limpar();
    console.log('pagina cadastrar');
    event.preventDefault();
    fetch('mc.html')
    .then( (response) => response.text())
    .then( (html) => document.querySelector('main').innerHTML = html )
    .catch((error)=>console.log(error))

    fetch('js/medicamentos.js')
    .then((response) => response.text())
    .then( (script) => {
        var tagScript = document.createElement('script');
        tagScript.innerHTML= script;
        document.body.appendChild(tagScript);
    })
    .catch((error)=>console.log(error));
    
});
document.getElementById('listagem').addEventListener('click', function(event){
    limpar();
    console.log('pagina listar');
    event.preventDefault();
    fetch('ml.html')
    .then(response=>response.text())
    .then((html)=>{
        document.querySelector('main').innerHTML=html;
    })
    .catch((erro)=>console.log(erro));

    fetch('js/listagemMedicamentos.js')
    .then(response=>response.text())
    .then(script=>{
        var tagScript = document.createElement('script');
        tagScript.innerHTML += script;
        document.body.appendChild(tagScript);
    });    
});

function carregaEditar(id){
    limpar();
    console.log('pagina editar');
    fetch(`me.html?id=${id}`)
    .then(response=>response.text())
    .then((html)=>{
        document.querySelector('main').innerHTML=html;
    })
    .catch((erro)=>console.log(erro));

    fetch('js/editarMedicamentos.js')
    .then(response=>response.text())
    .then(script=>{
        var tagScript = document.createElement('script');
        tagScript.innerHTML = `var id = ${id}; \n`;
        tagScript.innerHTML += script;
        document.body.appendChild(tagScript);
    });
}

document.getElementById('edicao').addEventListener('click', function(event){
    event.preventDefault();
});


