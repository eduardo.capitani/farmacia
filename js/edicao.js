function carregaLocalStorage(){
    return JSON.parse(localStorage.getItem('medicamentos'));
}

function salvar(event){
    event.preventDefault();
    var id = 1;
    console.log('salvando ...');
    var nome = document.getElementsByName('nome')[0].value;
    var composto = document.getElementsByName('composto')[0].value;
    var valor = document.getElementsByName('valor')[0].value;

    var medicamento = { 
        'id':id,
        'nome':nome,  
        'composto': composto,
        'valor': valor
    }
    var lista = carregaLocalStorage();    
    var novaLista = [];
    lista.forEach(e => {
        if(e['id'] != id){
            novaLista.push(e);
        }
        else{
            novaLista.push(medicamento);
        }
    });
    localStorage.setItem('medicamentos', JSON.stringify(novaLista));
    alert('salvo com sucesso')
}

function carregaCampos(dado){
    document.getElementsByName('id')[0].value = dado['id'];
    document.getElementsByName('nome')[0].value = dado['nome'];
    document.getElementsByName('composto')[0].value = dado['composto'];
    document.getElementsByName('valor')[0].value = dado['pessoa_contato'];
}
function carregaDados(){
    var urlParametros = new URLSearchParams(window.location.search);
    var id = parseInt(urlParametros.get('id'));
    var medicamentos = JSON.parse(localStorage.getItem('medicamentos'));
    medicamentos.forEach(e => {
        if(e['id'] === id) {
            carregaCampos(e);
        }
    });
}

window.onload = carregaDados();



