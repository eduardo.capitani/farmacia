function limpar(event){
    event.preventDefault();
    document.querySelector('form').reset();
}
function salvar(event){
    event.preventDefault();
    var id = 1;
    console.log('salvando ...');
    var nome = document.getElementsByName('nome')[0].value;
    var composto = document.getElementsByName('composto')[0].value;
    var valor = document.getElementsByName('valor')[0].value;
    
    var listaMedicamentos = JSON.parse(localStorage.getItem('medicamentos'));
    if(listaMedicamentos==null){
        listaMedicamentos=[];
    }else{
        var tamanhoLista = listaMedicamentos.length;
        var ultimoElemento = listaMedicamentos[tamanhoLista -1];
        var ultimoId = ultimoElemento['id'];
        id = ultimoId + 1
    }

    var medicamento = { 
        'id':id,
        'nome':nome,  
        'composto': composto,
        'valor': valor
    }
    listaMedicamentos.push(medicamento);
    localStorage.setItem('medicamentos', JSON.stringify(listaMedicamentos));
    limpar(event);
}
document.getElementById('salvar').addEventListener('click', salvar);

