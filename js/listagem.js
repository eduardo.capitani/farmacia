function carregaDados(){
    return JSON.parse(localStorage.getItem('medicamentos'));
}

function carregarEditar(event, id){
    console.log('Evento de click',event);
    event.preventDefault();
    carregaEditar(id);
};

function carregar(){
    var tbody = document.querySelector('tbody');
    tbody.innerHTML = '';
    var medicamentos = localStorage.getItem('medicamentos');
    medicamentos = JSON.parse(medicamentos);
    medicamentos.forEach( (e)=>{
        var tr = `<tr>
                    <td>${e['id']}</td>
                    <td>${e['nome']}</td>
                    <td>${e['composto']}</td>
                    <td>${e['valor']}</td>
                    <td>
                        <a href="me.html">Editar</a> |
                        <a href="">Deletar</a>
                    </td>
                </tr>`;
        tbody.innerHTML += tr;
    });   
}
function deletar(id){
    var lista = carregaDados();    
    var novaLista = [];
    lista.forEach(e => {
        if(e['id'] != id){
            novaLista.push(e);
        }
    });
    localStorage.setItem('medicamentos', JSON.stringify(novaLista));
    carregar();
}

window.onload = carregar();
